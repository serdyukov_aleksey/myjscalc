var output = document.getElementById("output-value");
var historyOutput = document.getElementById("history-value");
var number = document.getElementsByClassName("number");
var operator = document.getElementsByClassName("operator");
var isCleanOutput = false;
var operation = "";


// создаем лисенеры для кнопок с числами
for(var i =0;i<number.length;i++){
    number[i].addEventListener('click',function(event){
        debugger
        if (output.value === "0" || isCleanOutput === true){
            output.value = event.currentTarget.id;
            isCleanOutput = false;
        }else {

            output.value += event.currentTarget.id;
        }
    });
}

// создаем лисенеры для кнопок с операторами
for(i =0;i<operator.length;i++){

    operator[i].addEventListener('click',function(event){

        if(event.currentTarget.id==="clear"){
            output.value = "0";
            historyOutput.value ="";
            // printHistory("");
            // printOutput("");
        }
        else if(event.currentTarget.id==="backspace"){
            output.value = output.value.substr(0,output.value.length-1);

            if (output.value.length === 0){
                output.value = 0;
            }
        }
        else if(event.currentTarget.id==="+"){
            operation="+";
            historyOutput.value = output.value + operation;
            isCleanOutput = true;

        }else if(event.currentTarget.id==="-"){
            operation="-";
            historyOutput.value = output.value + operation;
            isCleanOutput = true;

        }else if(event.currentTarget.id==="*"){
            operation="*";
            historyOutput.value = output.value + operation;
            isCleanOutput = true;

        }else if(event.currentTarget.id==="/"){
            operation="/";
            historyOutput.value = output.value + operation;
            isCleanOutput = true;

        }else if(event.currentTarget.id==="="){
            var val1 = historyOutput.value.substr(0, historyOutput.value.length-1);
            var val2 = output.value;
            historyOutput.value += output.value;
            historyOutput.value += "=";
            output.value = calc(val1,val2,operation);
            isCleanOutput = true;
        }else if(event.currentTarget.id==="point"){
            if (output.value.indexOf(".")===-1){
                output.value += ".";
            }
        }else if(event.currentTarget.id==="%"){
            debugger
                output.value = historyOutput.value.substr(0, historyOutput.value.length-1) * output.value / 100;
        }

    });
}

function calc(val1, val2, operation) {
    var result = 0;
    let num1 = Number(val1);
    let num2 = Number(val2);
    switch (operation) {
        case "+": result = num1 + num2; break;
        case "-": result = num1 - num2; break;
        case "/": result = num1 / num2; break;
        case "*": result = num1 * num2; break;
    }
    return result;
}
